/*
 * (C) Copyright : 2018
 * Andrei Bogdan-Aleanxru, 336 CB, Tema 3 SO Linux
 *
 * Loader Implementation
 *
 * 2018, Operating Systems
 */

#define DIE(assertion, call_description)				\
	do {								\
		if (assertion) {					\
			fprintf(stderr, "(%s, %d): ",			\
					__FILE__, __LINE__);		\
			perror(call_description);			\
			exit(EXIT_FAILURE);				\
		}							\
	} while (0)

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#include "exec_parser.h"

static so_exec_t *exec;
struct sigaction action, old_action;
int exec_fd; // FileDescriptor pentru executabil

/*	Atasam un vector campului (void *data) din fiecare segment
 * in care vom tine  minte ce pagini am mapat deja pe un segment.
 */
struct page {
	char *mem_addr; // Adresa din memorie la care am mapat pagina
	uintptr_t addr; // Adresa tinuta ca int, pentru calcule
	int pageSize; // Dimensiunea paginii mapate
} page;

/* Se vor copia in memorie (dst), [file_size] bytes din executabil (exec_fd) */
static void copy_data(char *dst, long file_size, long offset)
{
	int rt;
	char *src;
	int pageSize = getpagesize();

	/* Nu exista date in executabil ce ar trebui copiate */
	if (file_size <= 0)
		return;

	/* Nu copiem mai mult de o pagina */
	if (file_size > pageSize)
		file_size = pageSize;

	/* Mapam fisierul in memorie */
	src = mmap(NULL, file_size, PROT_READ, MAP_PRIVATE, exec_fd, offset);
	DIE(src == MAP_FAILED, "mmap src");

	/* Copiem datele in memorie */
	memcpy(dst, src, file_size);

	/* Am copiat datele in memorie. Inchidem maparea fisierului */
	rt = munmap(src, file_size);
	DIE(rt == -1, "munmap executable");
}

/*	Umplem cu 0 memoria ce nu face parte dintr-un file_size al unui segment,
 * dar care face parte din mem_size. Astfel exista 3 cazuri de tratat:
 *
 *			[_page_addr_0][_page_addr_1][_page_addr_2][_page_addr_3]
 *			[____ file_size _____]
 *	vaddr ->[_____________________ mem_size ________________]
 */
static void set_0_memory(uintptr_t vaddr, long file_size,
	long mem_size, int page_addr)
{
	int pageSize = getpagesize();
	int file_end = vaddr + file_size;
	int mem_end = vaddr + mem_size;
	int page_end = page_addr + pageSize;

	/* Cu siguranta nu exista memorie [mem_size] de setat pe 0 */
	if (file_size == mem_size)
		return;

	/* Cazul 0 - suntem in file_size, nu avem zona de setat pe 0*/
	if (page_end < file_end)
		return;

	/* Cazul 1 - partea dreapta a paginii trebuie setata pe 0 */
	if (page_addr < file_end && page_end > file_end
				&& page_end < mem_end)
		memset((void *) file_end, 0, file_end - page_addr);

	/* Cazul 2 - toata pagina trebuie setata pe 0 */
	else if (page_addr > file_end && page_end < mem_end)
		memset((void *) page_addr, 0, pageSize);

	/* Cazul 3 - partea stanga a paginii trebuie setata pe 0 */
	else if (page_addr < mem_end && page_end > mem_end)
		memset((void *) page_addr, 0, mem_end - page_addr);
}

/*	Decodificam ce permisiuni avem pentru un anumit segment si
 * returnam o valoare folosing constantele din <mman.h>
 */
static unsigned int decode_prot(unsigned int p)
{
	unsigned int prot = 0;

	if (p / 4) {
		p %= 4;
		prot |= PROT_EXEC;
	}
	if (p / 2) {
		p %= 2;
		prot |= PROT_WRITE;
	}
	if (p / 1) {
		p %= 1;
		prot |= PROT_READ;
	}

	return prot;
}

/* Handler personal pentru tratarea semnalelor */
static void segv_handler(int signum, siginfo_t *info, void *context)
{
	/*	Daca nu avem SIGSEGV, rulam handler default
	 * pentru semnalul primit si iesim
	 */
	if (signum != SIGSEGV) {
		old_action.sa_sigaction(signum, info, context);
		return;
	}

	int rt;
	char *dst;
	int pageSize = getpagesize(), page_no;
	uintptr_t page_addr;
	struct page *segment_pages;
	so_seg_t segment;

	/* Obtinem adresa din memorie din care a rezultat page fault */
	uintptr_t fault_addr = (uintptr_t)info->si_addr;

	/* Cautam segmentul din care face parte aceasta adresa */
	for (int seg = 0; seg < exec->segments_no; ++seg) {
		segment = exec->segments[seg];
		segment_pages = segment.data;

		/* Verificam daca (fault_addr) este in segmentul (seg) */
		if (fault_addr < segment.vaddr ||
			fault_addr - segment.vaddr > segment.mem_size)
			continue;

		/* Numarul paginii din segment din care face parte adresa */
		page_no = (fault_addr - segment.vaddr) / pageSize;

		/* (fault_addr) nu este in segmentul (seg) */
		if (fault_addr > segment.vaddr + segment.mem_size)
			continue;

		/*	Daca pagina fusese deja alocata,
		 * avem un acces invalid la memorie
		 */
		if (segment_pages[page_no].pageSize > 0) {
			old_action.sa_sigaction(signum, info, context);
			return;
		}

		/*	Accesul se face la o adresa noua. Mapam o noau pagina in
		 * functie de numarul paginii
		 */
		page_addr = segment.vaddr + (page_no * pageSize);
		dst = mmap((void *)page_addr, pageSize,
				PROT_READ  | PROT_WRITE | PROT_EXEC,
				MAP_SHARED | MAP_FIXED | MAP_ANONYMOUS, 0, 0);
		DIE(dst == MAP_FAILED, "mmap");

		/*	Copiem datele [file_size] din acest segment (file)
		 * in noua destinatie (dst)
		 */
		copy_data(dst, segment.file_size - (page_no * pageSize),
				segment.offset + (page_no * pageSize));
		/* Setam pe 0 memoria din mem_size, daca este cazul */
		set_0_memory(segment.vaddr, segment.file_size,
				segment.mem_size, page_addr);

		/*	Setam permisiunile conform
		 * cu cele ce corespund segmentului
		 */
		rt = mprotect(dst, pageSize, decode_prot(segment.perm));
		DIE(rt == -1, "mprotect dst");

		/* Salvam informatia pentru noua pagina */
		segment_pages[page_no].mem_addr = dst;
		segment_pages[page_no].addr = page_addr;
		segment_pages[page_no].pageSize = pageSize;

		return;
	}

	old_action.sa_sigaction(signum, info, context);
}

/*	Salvam in pointerul data informatii despre fiecare pagina
 * din fiecare segment
 */
static void init_pages_info(void)
{
	int pages_number;
	struct page *pages;

	for (int seg = 0; seg < exec->segments_no; ++seg) {
		/* Aflam numarul de pagini pentru care vom pastra informatii */
		pages_number = exec->segments[seg].mem_size/getpagesize()+1;

		/*	Daca dimensiunea segmentului este divizibila
		 * cu [pageSize] atunci avem o pagina in plus calculata
		 */
		if (exec->segments[seg].mem_size % getpagesize() == 0)
			pages_number--;

		/*	Alocam un vector in care pastram informatia legata
		 * de fiecare page dintr-un anumit segment
		 * pageSize 0 = pagina nemapata
		 */
		pages = malloc(pages_number * sizeof(struct page));
		for (int page = 0; page < pages_number; ++page)
			pages[page].pageSize = 0;

		/* Atasam vectorul segmentului folosind pointerul */
		exec->segments[seg].data = pages;
	}
}


/*	Acelasi cod / explicatii ca in `init_pages_info`. Demapam / dealocam tot
 * ce am folosit in executia programului.
 */
static void delete_page_info(void)
{
	int pages_number, rt;
	struct page *pages;
	so_seg_t segment;

	for (int seg = 0; seg < exec->segments_no; ++seg) {
		segment = exec->segments[seg];

		pages_number = segment.mem_size/getpagesize()+1;
		if (segment.mem_size % getpagesize() == 0)
			pages_number--;

		/* Vom lua pagina cu pagina si vom demapa tot */
		pages = segment.data;
		for (int page = 0; page < pages_number; ++page)
			if (pages[page].pageSize > 0) {
				rt = munmap(pages[page].mem_addr,
						pages[page].pageSize);
				DIE(rt == -1, "munmap");
			}
		/* Stergem vectorul de informatii din semgnetul curent */
		free(pages);
	}
}

/* Resetam handler-ul pentru semnale la cel default */
static void restore_signal(void)
{
	int rc;

	action.sa_sigaction = old_action.sa_sigaction;
	sigemptyset(&action.sa_mask);
	sigaddset(&action.sa_mask, SIGSEGV);
	action.sa_flags = SA_SIGINFO;

	rc = sigaction(SIGSEGV, &action, NULL);
	DIE(rc == -1, "sigaction");
}

/* Setam Handler-ul nou pentru semnale */
int so_init_loader(void)
{
	int rc;

	action.sa_sigaction = segv_handler;
	sigemptyset(&action.sa_mask);
	sigaddset(&action.sa_mask, SIGSEGV);
	action.sa_flags = SA_SIGINFO;

	rc = sigaction(SIGSEGV, &action, &old_action);
	DIE(rc == -1, "sigaction");

	return 0;
}

int so_execute(char *path, char *argv[])
{
	int rt;

	exec = so_parse_exec(path);
	if (!exec)
		return -1;

	/*	Setam si initalizam un vector cu informatii despre
	 * paginile din fiecare segment ce vor fi mapate
	 */
	init_pages_info();

	/* Deschidem executabilul pentru a citi datele */
	exec_fd = open(path, O_RDONLY);
	DIE(exec_fd == -1, "open executable");

	so_start_exec(exec, argv);

	/* Inchidem fd-ul executabilului */
	rt = close(exec_fd);
	DIE(rt == -1, "close executable");

	/* Stergem vecotrul de informatii din fiecare segment */
	delete_page_info();

	/* Nu vom mai folosi handler-ul personal */
	restore_signal();

	return 0;
}
